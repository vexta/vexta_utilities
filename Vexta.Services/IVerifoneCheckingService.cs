﻿using System;
using System.Collections.Generic;
using Vexta.Entities;

namespace Vexta.Services
{
    public interface IVerifoneCheckingService
    {
        List<PossibleTransaction> CompareVerifoneWithTransactions(DateTime? fromDate, DateTime? toDate, List<string> terminalIds, List<int> paymentMethodIds, List<int> states);
    }
}