﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Vexta.Services
{
    public interface IVerifoneTransactionService
    {
        Task GetVerifoneTransactionsAsync(Stream fileStream);
    }
}