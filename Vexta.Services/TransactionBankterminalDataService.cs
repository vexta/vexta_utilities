﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vexta.Entities;
using Vexta.State;
using Vexta.Utilities.Data;

namespace Vexta.Services
{
    public class TransactionBankterminalDataService : ITransactionBankterminalDataService
    {
        private readonly IRepository<TransactionBankterminalData> _repository;
        private readonly TransactionBankterminalDataState _transactionBankterminalDataState;
        public TransactionBankterminalDataService(IPostgresRepositoryFactory postgresRepositoryFactory, TransactionBankterminalDataState transactionBankterminalDataState)
        {
            _repository = postgresRepositoryFactory.Create<TransactionBankterminalData>();
            _transactionBankterminalDataState = transactionBankterminalDataState;
        }
    }
}
