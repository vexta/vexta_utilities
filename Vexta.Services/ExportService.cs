﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Vexta.Services
{
    public class ExportService : IExportService
    {
        public void GenericListToExcel<T>(List<T> query, string fileName)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("VerifoneTransactionsCheckingResult");

                // Get column headings
                var t = typeof(T);
                var headings = t.GetProperties();

                for (int i = 0; i < headings.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = headings[i].Name;
                }

                // Populate data
                if (query.Count() > 0)
                {
                    worksheet.Cells["A2"].LoadFromCollection(query);
                }

                // Format the header
                using (ExcelRange range = worksheet.Cells["A1:BZ1"])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                    range.Style.Font.Color.SetColor(Color.White);
                }

                worksheet.Column(4).Style.Numberformat.Format = "yyyy-MM-dd HH:mm:ss";
                worksheet.Column(11).Style.Numberformat.Format = "yyyy-MM-dd HH:mm:ss";
                worksheet.Column(12).Style.Numberformat.Format = "yyyy-MM-dd HH:mm:ss";

                worksheet.Column(13).Hidden = true;
                worksheet.Column(14).Hidden = true;

                package.SaveAs(new System.IO.FileInfo($"c:\\temp\\excel\\{fileName}.xlsx"));
                package.Dispose();
            }
        }
    }
}
