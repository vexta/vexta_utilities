﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Vexta.Entities;
using Vexta.State;

namespace Vexta.Services
{
    public class VerifoneTransactionService : IVerifoneTransactionService
    {
        private readonly VerifoneTransactionState _verifoneTransactionState;

        public VerifoneTransactionService(VerifoneTransactionState verifoneTransactionState)
        {
            _verifoneTransactionState = verifoneTransactionState;
        }
        
        public async Task GetVerifoneTransactionsAsync(Stream fileStream)
        {
            List<VerifoneTransaction> verifoneTransactions = new List<VerifoneTransaction>();

            await Task.Run(() =>
            {             
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(fileStream))
                {
                    var worksheet = package.Workbook.Worksheets[0];

                    // loop all rows
                    for (int row = worksheet.Dimension.Start.Row; row <= worksheet.Dimension.End.Row; row++)
                    {
                        var transaction = new VerifoneTransaction();
                        
                        // loop all columns
                        for (int col = worksheet.Dimension.Start.Column; col <= worksheet.Dimension.End.Column; col++)
                        {                            
                            // add the cell data to the list
                            if (worksheet.Cells[row, col].Value != null)
                            {
                                 switch (col)
                                {
                                    case 1:
                                        int.TryParse(worksheet.Cells[row, col].Value.ToString(), out var bax);
                                        transaction.Bax = bax;
                                        break;
                                    case 2:
                                        int.TryParse(worksheet.Cells[row, col].Value.ToString(), out var terminalId);
                                        transaction.TerminalId = terminalId;
                                        break;
                                    case 3:
                                        DateTime.TryParse(worksheet.Cells[row, col].Value.ToString(), out var dateTimeObject);
                                        transaction.TransactionTime = dateTimeObject;
                                        break;
                                    case 4:
                                        decimal.TryParse(worksheet.Cells[row, col].Value.ToString(), out var totalAmount);
                                        transaction.TotalAmount = totalAmount;
                                        break;
                                    case 5:
                                        decimal.TryParse(worksheet.Cells[row, col].Value.ToString(), out var purchaseAmount);
                                        transaction.PurchaseAmount = purchaseAmount;
                                        break;
                                    case 6:
                                        transaction.TruncatedPan = worksheet.Cells[row, col].Value.ToString();                                        
                                        break;
                                    case 7:
                                        transaction.PaymentType = worksheet.Cells[row, col].Value.ToString();                                        
                                        break;
                                    case 8:
                                        int.TryParse(worksheet.Cells[row, col].Value.ToString(), out var settlementNumber);
                                        transaction.SettlementNumber = settlementNumber;                                        
                                        break;
                                    case 9:
                                        transaction.StanRef = worksheet.Cells[row, col].Value.ToString();
                                        //int.TryParse(worksheet.Cells[row, col].Value.ToString(), out var stanRef);
                                        //transaction.StanRef = stanRef;                                        
                                        break;
                                    case 10:                                        
                                        transaction.AuthorizationCode = worksheet.Cells[row, col].Value.ToString();
                                        //int.TryParse(worksheet.Cells[row, col].Value.ToString(), out var autCode);
                                        //transaction.AuthorizationCode = autCode;                                                                                
                                        break;
                                    default:
                                        break;
                                }
                            }                                                     
                        }

                        transaction.ExternalTransactionId = string.Join("", transaction.StanRef, transaction.AuthorizationCode);

                        //int.TryParse(string.Join("", transaction.StanRef.ToString(), transaction.AuthorizationCode.ToString()), out var externalTransId);
                        //transaction.ExternalTransactionId = externalTransId;

                        verifoneTransactions.Add(transaction);
                    }                                 
                }
            });

            _verifoneTransactionState.SetVerifoneTransactions(verifoneTransactions);
        }
    }
}
