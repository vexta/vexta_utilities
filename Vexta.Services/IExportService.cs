﻿using System.Collections.Generic;

namespace Vexta.Services
{
    public interface IExportService
    {
        void GenericListToExcel<T>(List<T> query, string FileName);
    }
}