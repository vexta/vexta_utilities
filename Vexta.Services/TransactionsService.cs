﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vexta.Entities;
using Vexta.State;
using Vexta.Utilities.Data;

namespace Vexta.Services
{
    public class TransactionsService : ITransactionsService
    {
        private readonly IRepository<Transactions> _repository;
        private readonly TransactionsState _transactionsState;

        public TransactionsService(IPostgresRepositoryFactory postgresRepositoryFactory, TransactionsState transactionsState)
        {
            _repository = postgresRepositoryFactory.Create<Transactions>();
            _transactionsState = transactionsState;
        }
    }
}
