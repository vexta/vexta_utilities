﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vexta.Entities;
using Vexta.State;

namespace Vexta.Services
{
    public class VerifoneCheckingService : IVerifoneCheckingService
    {
        private readonly VerifoneTransactionState _verifoneState;
        private readonly TransactionsState _transactionsState;
        private readonly TransactionBankterminalDataState _transactionBankterminalDataState;
        private readonly JoinedTransactionAndBankterminalDataState _joinedTransactionAndBankterminalDataState;
        private List<PossibleTransaction> _possibleTransactions = new List<PossibleTransaction>();
        private int _transactionsCount;
        private int _verifoneTransactionsCount;

        public VerifoneCheckingService(VerifoneTransactionState verifoneState, TransactionsState transactionsState, TransactionBankterminalDataState transactionBankterminalDataState, JoinedTransactionAndBankterminalDataState joinedTransactionAndBankterminalDataState)
        {
            _verifoneState = verifoneState;
            _transactionsState = transactionsState;
            _transactionBankterminalDataState = transactionBankterminalDataState;
            _joinedTransactionAndBankterminalDataState = joinedTransactionAndBankterminalDataState;
        }

        private void JoinTransactionAndTransactionBankterminalData(DateTime? fromDate, DateTime? toDate, List<string> terminalIds, List<int> paymentMethodIds, List<int> states)
        {
            var transactions = _transactionsState.Transactions
                .Where(t => t.CreatedAt >= fromDate && t.CreatedAt <= toDate && paymentMethodIds.Contains(t.PaymentMethodId) && states.Contains(t.State)).ToList();

            _transactionsCount = transactions.Count;
            
            var bankterminalDatas = _transactionBankterminalDataState.TransactionBankterminalDatas
                .Where(tbd => tbd.TransactionTimestamp >= fromDate && tbd.TransactionTimestamp <= toDate && terminalIds.Contains(tbd.TerminalId)).ToList();

            var result = transactions
                .SelectMany(t => bankterminalDatas
                .Where
                    (
                        bd => bd.TransactionId == t.Id
                    )
                    .Select(s => new JoinedTransactionAndBankterminalData
                    {
                        TransactionId = t.Id,
                        TransactionAmount = t.Amount,
                        PaymentMethodId = t.PaymentMethodId,
                        State = t.State,
                        CreatedAt = t.CreatedAt.Value,
                        CreatedAtUTC = t.CreatedAt.Value.AddHours(1),
                        CapturedAt = t.CapturedAt.Value,
                        Reason = t.Reason,
                        AccountNumber = s.AccountNumber,
                        TransactionTimestamp = s.TransactionTimestamp,
                        SessionNumber = s.SessionNumber,
                        ReferenceNumber = s.RetrievalReferenceNumber,
                        TerminalId = s.TerminalId,
                        PaymentType = s.ApplicationLabel,
                        TerminaldataAmount = s.TotalAmount
                    }));

            _joinedTransactionAndBankterminalDataState.SetJoinedTransactionAndBankterminalData(result.ToList());
        }

        public List<PossibleTransaction> CompareVerifoneWithTransactions(DateTime? fromDate, DateTime? toDate, List<string> terminalIds, List<int> paymentMethodIds, List<int> states)
        {
            JoinTransactionAndTransactionBankterminalData(fromDate, toDate, terminalIds, paymentMethodIds, states);

            var verifoneTransactions = _verifoneState.VerifoneTransactions.Where(v => v.TransactionTime >= fromDate && v.TransactionTime <= toDate && terminalIds.Contains(v.TerminalId.ToString())).ToList();

            _verifoneTransactionsCount = verifoneTransactions.Count;
            
            var joinedList = _joinedTransactionAndBankterminalDataState.JoinedTransactionAndBankterminalDatas.ToList();

            var verifoneTransactionsWithNoMatch = verifoneTransactions.Where(v => joinedList.All(j => !j.ReferenceNumber.Contains(v.ExternalTransactionId))).ToList();

            return FindPossibleTransactionsThatMatchVerifoneTransactions(verifoneTransactionsWithNoMatch, fromDate, toDate, paymentMethodIds);
        }

        private List<PossibleTransaction> FindPossibleTransactionsThatMatchVerifoneTransactions(List<VerifoneTransaction> verifoneTransactions, DateTime? fromDate, DateTime? toDate, List<int> paymentMethodIds)
        {
            _possibleTransactions.Clear();

            var transactions = _transactionsState.Transactions
                .Where(t => t.CreatedAt >= fromDate && t.CreatedAt <= toDate && paymentMethodIds.Contains(t.PaymentMethodId)).ToList();

            foreach (var verifoneTransaction in verifoneTransactions)
            {                
                var possibleTransactions = transactions.Where(t => t.Amount == verifoneTransaction.TotalAmount && Math.Abs((t.CreatedAt.Value - verifoneTransaction.TransactionTime.Value.ToUniversalTime()).TotalSeconds) <= 8 && t.State != 20).ToList();

                if (possibleTransactions.Count == 1)
                {
                    var possibleTransaction = possibleTransactions.First();
                    _possibleTransactions.Add
                        (
                            new PossibleTransaction
                            {
                                PaymentMethodId = possibleTransaction.PaymentMethodId,
                                TransactionId = possibleTransaction.Id,
                                TransactionAmount = possibleTransaction.Amount,
                                TransactionCreatedAt = possibleTransaction.CreatedAt,
                                TransactionCapturedAt = possibleTransaction.CapturedAt,
                                TransactionState = possibleTransaction.State,
                                TransactionReason = possibleTransaction.Reason,
                                VerifoneTerminalId = verifoneTransaction.TerminalId,
                                VerifoneTransactionTime = verifoneTransaction.TransactionTime,
                                VerifoneTotalAmount = verifoneTransaction.TotalAmount,
                                VerifoneStanRef = verifoneTransaction.StanRef,
                                VerifoneAuthCode = verifoneTransaction.AuthorizationCode,
                                VerifoneTransactionsCount = _verifoneTransactionsCount,
                                TransactionsCount = _transactionsCount
                            }
                        );
                }
                else
                {
                    _possibleTransactions.Add
                        (
                            new PossibleTransaction
                            {                                
                                VerifoneTerminalId = verifoneTransaction.TerminalId,
                                VerifoneTransactionTime = verifoneTransaction.TransactionTime,
                                VerifoneTotalAmount = verifoneTransaction.TotalAmount,
                                VerifoneStanRef = verifoneTransaction.StanRef,
                                VerifoneAuthCode = verifoneTransaction.AuthorizationCode,
                                VerifoneTransactionsCount = _verifoneTransactionsCount,
                                TransactionsCount = _transactionsCount
                            }
                        );
                }
            }

            return _possibleTransactions;
        }
    }
}
