﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace RedirectParser
{
    public class CsvHandler<T> 
    {
        private readonly IEnumerable<T> _objectsToWrite;

        public CsvHandler(IEnumerable<T> objectsToWrite)
        {
            _objectsToWrite = objectsToWrite;
        }

        public async Task WriteCsv(string filename)
        {
            using (var writer = new StreamWriter($"c:\\temp\\csv\\{filename}_{DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")}.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                await csv.WriteRecordsAsync(_objectsToWrite);
            }
        }
    }
}
