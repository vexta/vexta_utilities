﻿namespace RedirectParser
{
    public class PointsOfSale
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string Subdomain { get; set; }
    }
}