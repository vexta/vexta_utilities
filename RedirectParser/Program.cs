﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using Vexta.Utilities.Data;
using Vexta.Utilities.Data.Postgres;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;
using System.IO;
using Npgsql;
using Dapper;
using System.Collections.Generic;

namespace RedirectParser
{
    class Program
    {
        //static Task Main(string[] args) => CreateHostBuilder(args).Build().RunAsync();

        //private static IHostBuilder CreateHostBuilder(string[] args) => Host.CreateDefaultBuilder(args);

        private static IEnumerable<Company> _companies;
        private static IEnumerable<Redirect> _redirects;
        private static IEnumerable<PointsOfSale> _pointsOfSale;
        private static List<Redirect> _redirectList = new List<Redirect>();

        static async Task Main(string[] args)
        {
            var apiConnection = InitOptions<DbConnection>("ApiConnection");
            var redirectConnection = InitOptions<DbConnection>("RedirectConnection");

            var apiSql = "SELECT id, name, subdomain FROM companies; SELECT id, company_id as companyId, subdomain FROM points_of_sale;";
            var redirectSql = "SELECT \"Id\", \"Key\", \"Target\", \"Permanent\" FROM \"Redirects\";";

            var postgresConnectionString = GetPostgresConnectionString(apiConnection);

            using (var apiConn = new NpgsqlConnection(postgresConnectionString))
            {
                await apiConn.OpenAsync();

                using (var multi = await apiConn.QueryMultipleAsync(apiSql))
                {
                    _companies = multi.Read<Company>().ToList();
                    _pointsOfSale = multi.Read<PointsOfSale>().ToList();
                }

                await apiConn.CloseAsync();
            };

            using (var redirectConn = new NpgsqlConnection(GetPostgresConnectionString(redirectConnection)))
            {
                _redirects = await redirectConn.QueryAsync<Redirect>(redirectSql);
            }

            foreach (var redirect in _redirects)
            {
                var uri = new Uri(redirect.Target);
                var host = uri.Host;
                var subdomain = host.Split('.')[0];
                
                foreach (var pointOfSale in _pointsOfSale)
                {
                    if (!string.IsNullOrWhiteSpace(pointOfSale.Subdomain) && !string.IsNullOrWhiteSpace(subdomain))
                    {
                        if (pointOfSale.Subdomain.ToLower().Equals(subdomain.ToLower()))
                        {
                            var companySubdomain = GetCompanySubdomain(pointOfSale.CompanyId);
                            var newUri = $"{uri.Scheme}://{host.Replace(subdomain, companySubdomain)}/points-of-sale/{pointOfSale.Id}{uri.AbsolutePath}";

                            _redirectList.Add(new Redirect { Id = redirect.Id, Key = redirect.Key, Permanent = redirect.Permanent, Target = newUri, OldTarget = redirect.Target });
                        }
                    }                    
                }
            }

            var csvHandler = new CsvHandler<Redirect>(_redirectList);
            await csvHandler.WriteCsv($"{nameof(_redirectList)}");            

            var _dumpedRedirects = _redirects.Except(_redirectList, new IdComparer()).ToList();

            var csvDumpedHandler = new CsvHandler<Redirect>(_dumpedRedirects);
            await csvDumpedHandler.WriteCsv($"{nameof(_dumpedRedirects)}");

            var sqlScriptHandler = new SqlScriptHandler();
            await sqlScriptHandler.GenerateAndWriteUpdateScript(nameof(_redirectList), _redirectList);

            Console.Read();
        }

        private static string GetCompanySubdomain(int companyId)
        {
            return _companies.Where(c => c.Id == companyId).Select(c => c.Subdomain).First();
        }

        private static T InitOptions<T>(string section) where T : new()
        {
            var config = InitConfig();

            var sec = config.GetSection(section).Get<T>();
            return sec;
            //return config.Get<T>();
        }

        private static IConfiguration InitConfig()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env}.json", true, true)
                .AddEnvironmentVariables();

            return builder.Build();
        }

        private static string GetPostgresConnectionString(DbConnection connection)
        {
            return $"Server={connection.Host};Port={connection.Port};Database={connection.Database};Userid={connection.Username};Password={connection.Password};CommandTimeout={connection.CommandTimeout};Timeout={connection.ConnectionTimeout}";
        }
    }
}
