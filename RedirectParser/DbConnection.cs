﻿namespace RedirectParser
{
    public class DbConnection
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int CommandTimeout { get; set; }
        public int ConnectionTimeout { get; set; }
    }
}