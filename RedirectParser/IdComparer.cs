﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace RedirectParser
{
    public class IdComparer : IEqualityComparer<Redirect>
    {
        public int GetHashCode(Redirect obj)
        {
            if (obj == null)
            {
                return 0;
            }

            return obj.Id.GetHashCode();
        }

        public bool Equals(Redirect x, Redirect y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (x is null || y is null)
            {
                return false;
            }

            return x.Id == y.Id;
        }        
    }
}
