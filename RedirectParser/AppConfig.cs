﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedirectParser
{
    public class AppConfig
    {
        public IEnumerable<DbConnection> ConnectionStrings { get; set; }
    }
}
