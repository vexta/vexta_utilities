﻿namespace RedirectParser
{
    public class Redirect
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string OldTarget { get; set; } = string.Empty;
        public string Target { get; set; }        
        public bool Permanent { get; set; }

    }
}