﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace RedirectParser
{
    public class SqlScriptHandler
    {
        public async Task GenerateAndWriteUpdateScript(string fileName, IEnumerable<Redirect> updateData)
        {
            await Task.Run(async () =>
            {
                var stringbuilder = new StringBuilder();
                stringbuilder.AppendLine($"update \"Redirects\" as u set");
                stringbuilder.AppendLine($"\"Target\" = u2.target");
                stringbuilder.AppendLine($"from (values");
                foreach (var item in updateData)
                {
                    stringbuilder.AppendLine($"({item.Id}, '{item.Target}'),");
                }
                stringbuilder.AppendLine($") as u2(id, target)");
                stringbuilder.AppendLine($"where u2.id = u.\"Id\"");

                var result = stringbuilder.ToString();
                var indexLastChar = result.LastIndexOf("),");

                result = result.Remove((indexLastChar) + 1, 1);

                //return result;

                await WriteUpdateScriptToFile(result, fileName);
            });
        }

        private async Task WriteUpdateScriptToFile(string text, string fileName)
        {
            await File.WriteAllTextAsync($"c:\\temp\\sqlscripts\\{fileName}_{DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")}.sql", text);
        }
    }
}
