﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RedirectParser
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Subdomain { get; set; }
    }
}