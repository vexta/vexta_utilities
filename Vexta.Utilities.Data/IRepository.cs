﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vexta.Utilities.Data
{
    public interface IRepository<T> : IDisposable
    {
        Task Add(T instance);
        Task<T> Get(int id);
        Task<IEnumerable<T>> List();
        Task Delete(int id);
        Task Update(int id, T instance);
        Task<IEnumerable<T>> Execute(string command, object parameters);
        IQueryable<T> Query();
    }
}
