﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Vexta.Utilities.Data
{
    public interface IDbConnectionFactory
    {
        IDbConnection Create();
    }
}
