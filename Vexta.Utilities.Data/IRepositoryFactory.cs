﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vexta.Utilities.Data
{
    public interface IRepositoryFactory
    {
        IRepository<T> Create<T>();
    }
}
