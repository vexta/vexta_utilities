﻿using Dapper;
using Microsoft.Extensions.Logging;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vexta.Extensions;

namespace Vexta.Utilities.Data.Postgres
{
    public class PostgresRepository<T> : IRepository<T>
    {
        private readonly string _connectionString;
        //private readonly ILogger _logger;
        private readonly string _typeName;
        private readonly IDbConnection _connection;

        //public PostgresRepository(string connectionString, ILogger logger)
        public PostgresRepository(string connectionString)
        {
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
            _connectionString = connectionString;
            //_logger = logger;
            _typeName = typeof(T).Name.ToSnakeCase();
            _connection = new NpgsqlConnection(_connectionString);
            _connection.Open();
        }

        public async Task Add(T instance)
        {
            throw new NotImplementedException();
        }

        public async Task Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<T>> Execute(string command, object parameters)
        {
            throw new NotImplementedException();
        }

        public async Task<T> Get(int id)
        {
            //return await _connection.QueryFirstOrDefaultAsync<T>($"{_typeName}Get", new { Id = id }, commandType: CommandType.StoredProcedure);

            string sql = $"SELECT * FROM {_typeName} WHERE id = @typeId;";
            //var result = await _connection.QueryFirstOrDefaultAsync<T>(sql, new { typeId = id });
            
            return await _connection.QueryFirstOrDefaultAsync<T>(sql, new { typeId = id});
        }

        public async Task<IEnumerable<T>> List()
        {
            string sql = $"SELECT * FROM {_typeName};";
            var result = await _connection.QueryAsync<T>(sql);
            return result;

            //return await _connection.QueryAsync<T>($"{_typeName}List", commandType: CommandType.StoredProcedure);
            //throw new NotImplementedException();
        }

        public IQueryable<T> Query()
        {
            throw new NotImplementedException();
        }

        public async Task Update(int id, T instance)
        {
            throw new NotImplementedException();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect reundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //dispose managed state (managed objects):
                    _connection.Close();
                    _connection.Dispose();
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.

                disposedValue = true;
            }
        }

        // override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SqlRepository()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
