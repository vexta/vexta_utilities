﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Vexta.Utilities.Data.Postgres
{
    public class PostgresRepositoryFactory : IPostgresRepositoryFactory
    {
        private readonly string _connectionString;
        //private readonly ILogger _logger;

        //public PostgresRepositoryFactory(string connectionString, ILogger logger)
        public PostgresRepositoryFactory(string connectionString)
        {
            _connectionString = connectionString;
            //_logger = logger;
        }

        public IRepository<T> Create<T>()
        {
            // return new PostgresRepository<T>(_connectionString, _logger);
            return new PostgresRepository<T>(_connectionString);
        }
    }
}
