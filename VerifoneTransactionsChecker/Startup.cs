using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Vexta.Extensions;
using Vexta.Middlewares;
using Vexta.Services;
using Vexta.State;
using Vexta.Utilities.Data.Postgres;

namespace VerifoneTransactionsChecker
{
    public class Startup
    {
        private IWebHostEnvironment _env;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor().AddCircuitOptions(options =>
            {
                if (_env.IsDevelopment())
                {
                    options.DetailedErrors = true;
                }
            });
            services.AddSingleton<IVerifoneTransactionService, VerifoneTransactionService>();
            services.AddSingleton<VerifoneTransactionState>();
            services.AddSingleton<TransactionBankterminalDataState>();
            services.AddSingleton<TransactionsState>();
            services.AddSingleton<JoinedTransactionAndBankterminalDataState>();
            services.AddSingleton<ITransactionsService, TransactionsService>();
            services.AddSingleton<ITransactionBankterminalDataService, TransactionBankterminalDataService>();
            services.AddSingleton<IVerifoneCheckingService, VerifoneCheckingService>();
            services.AddSingleton<IExportService, ExportService>();

            string postgressConnectionString = GetPostgressConnectionString();
            //services.AddVextaUtilities(() => new PostgresRepositoryFactory(postgressConnectionString, _loggerFactory.CreateLogger("PostgresLogger")));
            services.AddVextaUtilities(() => new PostgresRepositoryFactory(postgressConnectionString));            
        }

        private string GetPostgressConnectionString()
        {
            var section = Configuration.GetSection("ApiConnection");

            string server = section.GetValue<string>("host");
            string port = section.GetValue<string>("port");
            string database = section.GetValue<string>("database");
            string userName = section.GetValue<string>("username");
            string password = section.GetValue<string>("password");

            string auth = string.IsNullOrEmpty(userName) ? "" : $"{userName}:{password}@";

            return $"Server={server};Port={port};Database={database};Userid={userName};Password={password};";
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            _env = env;

            if (_env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseMiddleware<VextaUploadMiddleware>();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
