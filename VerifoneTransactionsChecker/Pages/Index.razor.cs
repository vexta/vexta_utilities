﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Vexta.Entities;
using Vexta.State;

namespace VerifoneTransactionsChecker.Pages
{
    public partial class Index : IDisposable
    {
        private protected List<PossibleTransaction> PossibleTransactions { get; private set; } = new List<PossibleTransaction>();        
        private protected DateTime? FromDate { get; private set; }
        private protected DateTime? ToDate { get; private set; }
        private protected List<string> TerminalIds { get; private set; }
        private protected List<int> PaymentMethodIds { get; private set; }
        private protected List<int> TransactionStates { get; private set; }

        private bool disposedValue;

        private protected void FromDateHandler(DateTime? fromDate)
        {
            FromDate = fromDate.Value;
            StateHasChanged();
        }

        private protected void ToDateHandler(DateTime? toDate)
        {
            ToDate = toDate.Value;
            StateHasChanged();
        }

        private protected void TransactionCheckerHandler(List<PossibleTransaction> possibleTransactions)
        {
            PossibleTransactions = possibleTransactions;
            StateHasChanged();
        }

        private protected void OnChange<T,U>(string value, T target, Expression<Func<U>> property)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
                
                if (propertyInfo == null)
                {
                    throw new ArgumentException("The lambda expression 'property' should point to a valid Property");
                }

                object instance = Activator.CreateInstance(propertyInfo.PropertyType);
                IList valueList = (IList)instance;

                //Find the item type for the List<T>
                var itemType = valueList.AsQueryable().ElementType;

                if (value.Contains(','))
                {
                    var tmpArray = value.Split(',');
                    foreach (var item in tmpArray)
                    {
                        valueList.Add(Convert.ChangeType(item, itemType));
                    }
                }
                else
                {
                    valueList.Add(Convert.ChangeType(value, itemType));
                }

                propertyInfo.SetValue(target, Convert.ChangeType(valueList, propertyInfo.PropertyType), null);

                StateHasChanged();
            }
        }

        private protected bool ShouldShowTransactionCheckResults()
        {
            if (PossibleTransactions.Count > 0)
            {
                return true;
            }

            return false;
        }

        #region Disposable
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //VerifoneTransactionState.OnChange -= VerifoneTransactionState_OnChange;
                    //TransactionsState.OnChange -= TransactionsState_OnChange;
                    //TransactionBankterminalDataState.OnChange -= TransactionBankterminalDataState_OnChange;
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~Index()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
