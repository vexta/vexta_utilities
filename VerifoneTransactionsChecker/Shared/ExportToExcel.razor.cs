﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Vexta.Entities;
using Vexta.Services;

namespace VerifoneTransactionsChecker.Shared
{
    public partial class ExportToExcel
    {
        [Inject]
        private protected IExportService ExportService { get; private set; }
        
        [Parameter]
        public List<PossibleTransaction> Data { get; set; }
        [Parameter]
        public string FileName { get; set; }

        protected void Export()
        {
            ExportService.GenericListToExcel(Data, FileName);
        }
    }
}
