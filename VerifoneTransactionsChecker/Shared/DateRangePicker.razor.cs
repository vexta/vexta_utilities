﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VerifoneTransactionsChecker.Shared
{
    public partial class DateRangePicker
    {
        [Parameter]
        public EventCallback<DateTime?> OnFromDateChanged { get; set; }
        [Parameter]
        public EventCallback<DateTime?> OnToDateChanged { get; set; }
        private protected DateTime? FromDateValue { get; set; }
        private protected DateTime? ToDateValue { get; set; }

        protected override Task OnInitializedAsync()
        {
            FromDateValue = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            ToDateValue = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            OnFromDateChanged.InvokeAsync(FromDateValue);
            OnToDateChanged.InvokeAsync(ToDateValue);
            StateHasChanged();
            return base.OnInitializedAsync();
        }

        private void OnFromDateChange(DateTime? value, string format)
        {
            FromDateValue = new DateTime(value.Value.Year, value.Value.Month, value.Value.Day, 00, 00, 00);
            OnFromDateChanged.InvokeAsync(value.Value);
            StateHasChanged();
        }

        private void OnToDateChange(DateTime? value, string format)
        {
            ToDateValue = new DateTime(value.Value.Year, value.Value.Month, value.Value.Day, 23, 59, 59);
            OnToDateChanged.InvokeAsync(ToDateValue.Value);
            StateHasChanged();
        }
    }
}
