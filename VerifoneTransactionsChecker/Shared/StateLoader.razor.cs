﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vexta.Entities;
using Vexta.State;
using Vexta.Utilities.Data;

namespace VerifoneTransactionsChecker.Shared
{
    public partial class StateLoader
    {
        [Inject]
        private protected IPostgresRepositoryFactory PostgresRepositoryFactory { get; private set; }
        [Inject]
        private protected TransactionsState TransactionsState { get; private set; }
        [Inject]
        private protected TransactionBankterminalDataState TransactionBankterminalDataState { get; private set; }
        [Parameter]
        public RenderFragment ChildContent { get; set; }
        
        //private protected bool hasLoaded;

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                var transactions = await PostgresRepositoryFactory.Create<Transactions>().List();
                var transactionBankterminalDatas = await PostgresRepositoryFactory.Create<TransactionBankterminalData>().List();

                TransactionsState.SetTransactions(transactions);
                TransactionBankterminalDataState.SetTransactionBankterminalData(transactionBankterminalDatas);

                //hasLoaded = true;
            }            
        }
    }
}
