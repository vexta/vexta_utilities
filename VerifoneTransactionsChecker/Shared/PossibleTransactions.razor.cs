﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vexta.Entities;
using Vexta.State;

namespace VerifoneTransactionsChecker.Shared
{
    public partial class PossibleTransactions
    {        
        [Parameter]
        public List<PossibleTransaction> PossibleTransactionsList { get; set; }
        
        private protected List<PossibleTransaction> PossibleTransactionResults { get; private set; }
        private protected string NumberOfTransactions { get; private set; }
        private protected string NumberOfVerifoneTransactions { get; private set; }

        protected override Task OnParametersSetAsync()
        {
            PossibleTransactionResults = PossibleTransactionsList;
            NumberOfTransactions = PossibleTransactionsList.Select(t => t.TransactionsCount.ToString()).FirstOrDefault();
            NumberOfVerifoneTransactions = PossibleTransactionsList.Select(t => t.VerifoneTransactionsCount.ToString()).FirstOrDefault();
            StateHasChanged();

            return base.OnInitializedAsync();
        }
    }
}
