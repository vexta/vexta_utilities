﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vexta.Entities;
using Vexta.Services;

namespace VerifoneTransactionsChecker.Shared
{
    public partial class TransactionChecker
    {
        [Inject]
        private protected IVerifoneCheckingService VerifoneCheckingService { get; private set; }
        [Parameter]
        public DateTime? FromDate { get; set; }
        [Parameter]
        public DateTime? ToDate { get; set; }
        [Parameter]
        public List<string> TerminalIds { get; set; }
        [Parameter]
        public List<int> PaymentMethodIds { get; set; }
        [Parameter]
        public List<int> States { get; set; }
        [Parameter]
        public EventCallback<List<PossibleTransaction>> OnTransactionCheckerChanged { get; set; }
        
        void OnClick()
        {
            var possibleTransactions = VerifoneCheckingService.CompareVerifoneWithTransactions(FromDate, ToDate, TerminalIds, PaymentMethodIds, States);
            OnTransactionCheckerChanged.InvokeAsync(possibleTransactions);
            StateHasChanged();            
        }
    }
}
