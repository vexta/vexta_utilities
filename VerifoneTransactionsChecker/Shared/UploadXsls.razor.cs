﻿using Microsoft.AspNetCore.Components;
using Radzen;
using Radzen.Blazor;
using Vexta.Services;
using Vexta.State;

namespace VerifoneTransactionsChecker.Shared
{
    public partial class UploadXsls
    {
        [Inject]
        private protected VerifoneTransactionState VerifoneTransactionState { get; private set; }

        RadzenUpload upload;
        int progress;
        string info;

        void OnChange(UploadChangeEventArgs args, string name)
        {
            foreach (var file in args.Files)
            {
                //console.Log($"File: {file.Name} / {file.Size} bytes.";
            }

            //console.Log($"{name} changed");
        }

        void OnProgress(UploadProgressArgs args, string name)
        {
            this.info = $"% '{name}' & {args.Loaded} of {args.Total} bytes.";
            this.progress = args.Progress;

            if (args.Progress == 100)
            {
                //console.Clear();

                //foreach (var file in args.Files)
                //{
                //    //console.Log("$Uploaded: {file.Name} / {file.Size} bytes");
                //}
                var verifoneTransactions = VerifoneTransactionState.VerifoneTransactions;
            }
        }

        void OnComplete(UploadCompleteEventArgs args)
        {
            //console.Log($"Server response: {args.RawResponse}");
        }
    }
}
