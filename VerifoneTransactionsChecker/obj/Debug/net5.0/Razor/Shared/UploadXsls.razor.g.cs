#pragma checksum "C:\Git\vexta_util\VerifoneTransactionsChecker\Shared\UploadXsls.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "347e5b433672fdc7825b70f8015cb216a4c37a26"
// <auto-generated/>
#pragma warning disable 1591
namespace VerifoneTransactionsChecker.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using VerifoneTransactionsChecker;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using VerifoneTransactionsChecker.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using Radzen;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Git\vexta_util\VerifoneTransactionsChecker\_Imports.razor"
using Radzen.Blazor;

#line default
#line hidden
#nullable disable
    public partial class UploadXsls : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "row");
            __builder.OpenElement(2, "div");
            __builder.AddAttribute(3, "class", "col-xl-6");
            __builder.AddMarkupContent(4, "<h3 style=\"margin-top: 1rem\">Upload Verifone Excel</h3>\r\n        ");
            __builder.OpenComponent<Radzen.Blazor.RadzenUpload>(5);
            __builder.AddAttribute(6, "Auto", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 4 "C:\Git\vexta_util\VerifoneTransactionsChecker\Shared\UploadXsls.razor"
                                          false

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(7, "Multiple", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 4 "C:\Git\vexta_util\VerifoneTransactionsChecker\Shared\UploadXsls.razor"
                                                           true

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(8, "Url", "upload/multiple");
            __builder.AddAttribute(9, "Style", "margin-bottom: 20px;");
            __builder.AddAttribute(10, "Change", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Radzen.UploadChangeEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Radzen.UploadChangeEventArgs>(this, 
#nullable restore
#line 7 "C:\Git\vexta_util\VerifoneTransactionsChecker\Shared\UploadXsls.razor"
                               args => OnChange(args, "Manual Upload")

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(11, "Progress", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Radzen.UploadProgressArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Radzen.UploadProgressArgs>(this, 
#nullable restore
#line 8 "C:\Git\vexta_util\VerifoneTransactionsChecker\Shared\UploadXsls.razor"
                                 args => OnProgress(args, "Manual Upload")

#line default
#line hidden
#nullable disable
            )));
            __builder.AddComponentReferenceCapture(12, (__value) => {
#nullable restore
#line 4 "C:\Git\vexta_util\VerifoneTransactionsChecker\Shared\UploadXsls.razor"
                            upload = (Radzen.Blazor.RadzenUpload)__value;

#line default
#line hidden
#nullable disable
            }
            );
            __builder.CloseComponent();
            __builder.AddMarkupContent(13, "\r\n\r\n        ");
            __builder.OpenComponent<Radzen.Blazor.RadzenButton>(14);
            __builder.AddAttribute(15, "Text", "Upload");
            __builder.AddAttribute(16, "Click", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 10 "C:\Git\vexta_util\VerifoneTransactionsChecker\Shared\UploadXsls.razor"
                                            args => upload.Upload()

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(17, "Style", "margin-bottom: 20px;");
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
