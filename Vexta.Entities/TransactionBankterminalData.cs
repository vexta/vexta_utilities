﻿using System;

namespace Vexta.Entities
{
    public class TransactionBankterminalData
    {
        private decimal _totalAmount;

        public int Id { get; set; }
        public int TransactionId { get; set; }
        public string AccountNumber { get; set; }
        public DateTime? TransactionTimestamp { get; set; }
        public string SessionNumber { get; set; }
        public string RetrievalReferenceNumber { get; set; }
        public string TerminalId { get; set; }
        public string ApplicationLabel { get; set; }
        //public decimal TotalAmount { get; set; } 
        public decimal TotalAmount
        {
            get
            {
                return _totalAmount;
            }
            set
            {
                //decimal.TryParse(value.ToString("0.00"), out decimal myDecimal);
                //_totalAmount = myDecimal;
                _totalAmount = value / 100;
            }
        }
    }
}
