﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vexta.Entities
{
    public class PossibleTransaction
    {
        public int TransactionId { get; set; } = 0;
        public decimal TransactionAmount { get; set; } = 0;
        public int PaymentMethodId { get; set; } = 0;
        public int TransactionState { get; set; } = 0;
        public string TransactionReason { get; set; } = string.Empty;
        public DateTime? TransactionCreatedAt { get; set; } = null;
        public DateTime? TransactionCapturedAt { get; set; } = null;
        public string VerifoneStanRef { get; set; }
        public string VerifoneAuthCode { get; set; }
        public decimal VerifoneTotalAmount { get; set; }
        public DateTime? VerifoneTransactionTime { get; set; }
        public int VerifoneTerminalId { get; set; }
        public int VerifoneTransactionsCount { get; set; }
        public int TransactionsCount { get; set; }
    }
}
