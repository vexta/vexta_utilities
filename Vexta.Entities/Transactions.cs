﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vexta.Entities
{
    public class Transactions
    {
        private decimal _amount;

        public int Id { get; set; }
        public int LocationId { get; set; }
        public decimal Amount 
        {
            get
            {
                return _amount;
            }
            set
            {
                //decimal.TryParse(value.ToString("0.00"), out decimal myDecimal);
                //_amount = myDecimal;
                _amount = value / 100;
            }
        }
        public int PaymentMethodId { get; set; }
        public int State { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? CapturedAt { get; set; }
        public string Reason { get; set; }
        public DateTime? RefundedAt { get; set; }
    }
}
