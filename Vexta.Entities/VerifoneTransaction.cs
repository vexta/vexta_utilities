﻿using System;

namespace Vexta.Entities
{
    public class VerifoneTransaction
    {
        public int Bax { get; set; }
        public int TerminalId { get; set; }
        public DateTime? TransactionTime { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PurchaseAmount { get; set; }
        public string TruncatedPan { get; set; }
        public string PaymentType { get; set; }
        public int SettlementNumber { get; set; }
        public string StanRef { get; set; }
        public string AuthorizationCode { get; set; }
        public string ExternalTransactionId { get; set; }
        public int TransactionId { get; set; }
    }
}
