﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vexta.Entities
{
    public class JoinedTransactionAndBankterminalData
    {
        public int TransactionId { get; set; }
        public decimal TransactionAmount { get; set; }
        public int PaymentMethodId { get; set; }
        public int State { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? CreatedAtUTC { get; set; }
        public DateTime? CapturedAt { get; set; }
        public string Reason { get; set; }
        public string AccountNumber { get; set; }
        public DateTime? TransactionTimestamp { get; set; }
        public string SessionNumber { get; set; }
        public string ReferenceNumber { get; set; }
        public string TerminalId { get; set; }
        public string PaymentType { get; set; }
        public decimal TerminaldataAmount { get; set; }
    }
}
