﻿using System;
using System.Collections.Generic;
using Vexta.Entities;

namespace Vexta.State
{
    public class VerifoneTransactionState
    {
        public List<VerifoneTransaction> VerifoneTransactions { get; private set; }

        public event Action OnChange;

        public void SetVerifoneTransactions(List<VerifoneTransaction> verifoneTransactions)
        {
            VerifoneTransactions = verifoneTransactions;

            NotifyStateChange();
        }

        private void NotifyStateChange()
        {
            OnChange?.Invoke();
        }
    }
}
