﻿using System;
using System.Collections.Generic;
using System.Text;
using Vexta.Entities;

namespace Vexta.State
{
    public class TransactionBankterminalDataState
    {
        public IEnumerable<TransactionBankterminalData> TransactionBankterminalDatas { get; set; }

        public event Action OnChange;

        public void SetTransactionBankterminalData(IEnumerable<TransactionBankterminalData> transactionBankterminalDatas)
        {
            TransactionBankterminalDatas = transactionBankterminalDatas;

            NotifyStateChange();
        }

        private void NotifyStateChange()
        {
            OnChange?.Invoke();
        }
    }
}
