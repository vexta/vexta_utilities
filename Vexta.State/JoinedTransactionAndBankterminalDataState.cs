﻿using System;
using System.Collections.Generic;
using System.Text;
using Vexta.Entities;

namespace Vexta.State
{
    public class JoinedTransactionAndBankterminalDataState
    {
        public IEnumerable<JoinedTransactionAndBankterminalData> JoinedTransactionAndBankterminalDatas { get; set; }

        public event Action OnChange;

        public void SetJoinedTransactionAndBankterminalData(IEnumerable<JoinedTransactionAndBankterminalData> joinedTransactionAndBankterminalDatas)
        {
            JoinedTransactionAndBankterminalDatas = joinedTransactionAndBankterminalDatas;

            NotifyStateChange();
        }

        private void NotifyStateChange()
        {
            OnChange?.Invoke();
        }
    }
}
