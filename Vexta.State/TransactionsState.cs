﻿using System;
using System.Collections.Generic;
using System.Text;
using Vexta.Entities;

namespace Vexta.State
{
    public class TransactionsState
    {
        public IEnumerable<Transactions> Transactions { get; set; }

        public event Action OnChange;

        public void SetTransactions(IEnumerable<Transactions> transactions)
        {
            Transactions = transactions;

            NotifyStateChange();
        }

        private void NotifyStateChange()
        {
            OnChange?.Invoke();
        }
    }
}
