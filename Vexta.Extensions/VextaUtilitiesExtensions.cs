﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Vexta.Utilities.Data;

namespace Vexta.Extensions
{
    public static class VextaUtilitiesExtensions
    {
        public static IServiceCollection AddVextaUtilities(this IServiceCollection ioc, Func<IPostgresRepositoryFactory> postgressRepositoryFactoryProvider)
        {
            ioc.AddTransient<IPostgresRepositoryFactory>(sp => postgressRepositoryFactoryProvider());
            return ioc;
        }
    }
}
