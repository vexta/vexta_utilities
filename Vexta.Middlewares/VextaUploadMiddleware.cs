﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Vexta.Services;

namespace Vexta.Middlewares
{
    public class VextaUploadMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IVerifoneTransactionService _verifoneTransactionService;

        //private readonly IHttpContextAccessor _accessor;

        //public VextaUploadMiddleware(RequestDelegate next, IHttpContextAccessor httpContextAccessor)
        public VextaUploadMiddleware(RequestDelegate next, IVerifoneTransactionService verifoneTransactionService)
        {
            _next = next;
            _verifoneTransactionService = verifoneTransactionService;
            //_accessor = httpContextAccessor;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            switch (context.Request.Method)
            {
                //case "GET":
                //    break;
                case "POST":
                    await HandleCommand(context);
                    break;
                default:
                    await _next.Invoke(context);
                    break;
            }
        }

        private async Task HandleCommand(HttpContext context)
        {
            var contentType = context.Request.ContentType;
            var headers = context.Request.Headers;
            if (context.Request.HasFormContentType)
            {
                var fileCount = context.Request.Form.Files.Count;
                var files = context.Request.Form.Files;

                if (files.Count > 0)
                {
                    foreach (var file in files)
                    {
                        await ReadFormFileAsync(file);
                        //context.Items.Add("VERIFONE_TRANSACTIONS", verifoneTransactions);
                        //_verifoneTransactionService.VerifoneTransactions = verifoneTransactions;
                    }
                }
            }
            await _next.Invoke(context);
        }

        private async Task ReadFormFileAsync(IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                await Task.CompletedTask;
            }

            using (var memoryStream = new MemoryStream())
            {
                await file.CopyToAsync(memoryStream).ConfigureAwait(false);
                await _verifoneTransactionService.GetVerifoneTransactionsAsync(memoryStream);
            }
        }
    }
}
